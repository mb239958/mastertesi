// da https://www.softprayog.in/tutorials/linux-process-execution-time

#include <stdio.h> 
#include <stdlib.h>      
#include <unistd.h>      
#include <time.h>      

main ()      
{      
    clock_t ct1, ct2;      
    int i;      

    // clock ritorna il tempo di CPU usato dal processo corrente
    // viene ritornato in "clock ticks" (clock_t)
    if ((ct1 = clock ()) == -1)  
        perror ("clock");      

    // qui vediamo quanti clock ticks si fanno al secondo
    printf ("CLOCKS_PER_SEC = %ld\n", CLOCKS_PER_SEC);      

    // loop per fare lavorare CPU
    for (i = 0; i < 10000000; i++)      
        ;  

    if ((ct2 = clock ()) == -1)      
        perror ("clock");      

    // diff � quanta CPU ho consumato in clock ticks
    // if CLOCKS_PER_SEC is 1,000,000, the value is in microseconds
    // To get the time in seconds one has to divide it by 1,000,000, 
    // or, to be more precise, divide it by the value defined by the constant, CLOCKS_PER_SEC
    // es. se diff = 52014 i secondi di CPU sono 0,052014
    // ad esempio su qs pc  con RADEON           0,021899 (meno della metà di sopra)
    printf ("ct1 = %ld, ct2 = %ld, diff = %ld\n", ct1, ct2, ct2 - ct1);      
}